//
//  YappaSDK.h
//  YappaSDK
//
//  Created by Maxi Cassola on 04/09/2020.
//  Copyright © 2020 Maxi Cassola. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Flurry.h"
//! Project version number for YappaSDK.
FOUNDATION_EXPORT double YappaSDKVersionNumber;

//! Project version string for YappaSDK.
FOUNDATION_EXPORT const unsigned char YappaSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <YappaSDK/PublicHeader.h>


