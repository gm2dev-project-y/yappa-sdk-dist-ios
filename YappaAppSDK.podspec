Pod::Spec.new do |s|
    s.name             = 'YappaAppSDK'
    s.version          = '1.0.17'
    s.summary          = 'Audio & Video commenting tool'
    s.description      = 'Bring the conversation to your mobile app'

    s.homepage         = 'https://yappaapp.com/'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Yappa' => 'yappasdk@yappaapp.com' }
    s.source           = { :git => 'https://bitbucket.org/gm2dev-project-y/yappa-sdk-dist-ios.git', :tag => s.version.to_s }

    s.ios.deployment_target = '12.0'
    s.swift_version = '5'

    # s.source_files = 'YappaSDK/Frameworks/**/*'

    s.ios.vendored_frameworks = 'YappaSDK.xcframework'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

    s.dependency "Material"
    s.dependency "ObjectMapper"
    s.dependency "AlamofireObjectMapper", '= 6.2.0'
    s.dependency "FlagPhoneNumber"
    s.dependency "AWSLogs"
    s.dependency 'Alamofire', '~> 5.0.0-rc.2'
    s.dependency 'Firebase/Messaging'
    s.dependency 'GoogleUtilities'
end
