## YAPPA SDK iOS RELEASE NOTE ##

### 1.0.0(33) - YappaAppSDK 1.0.17  11-24-2021 ###

[SDKAPP-124] - fixed password regex

### 1.0.0(32) - YappaAppSDK 1.0.16  11-19-2021 ###

[VARIETY] - Added variety contentId change fix.

### 1.0.0(31) - YappaAppSDK 1.0.15  11-5-2021 ###

[SDKAPP-122] - Added Flurry custom events

### 1.0.0(30) - YappaAppSDK 1.0.14  11-4-2021 ###

[SDKAPP-5]  - Push notifications are not displaying for any action
[SDKAPP-7]  - Welcome message icon has not the correct format
[SDKAPP-8]  - Added 9 to arg phone number hint text
[SDKAPP-25]  - Registration Optional inputs - Validation



### 1.0.0(29) - YappaAppSDK 1.0.13  10-19-2021 ###

[SDKAPP-97]  - Tapp on yapp from notification center not navigating to yapp
[SDKAPP-112] - Flurry integration
[Fix]        - Flurry module fix
[First RN]   - First release note
[pod GoogleUtilities]   - Needed for Flurry

